package com.helix.genericgcm.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import junit.framework.Assert;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.helix.genericgcm.concrete.Parameters;

import android.util.Log;

public class RestClient {
private static final String TAG = "RestClient";	
	//General failure
	private static final int MINIMUM_SUCCESS_CODE 	=  200;
	private static final int MAXIMUM_SUCCESS_CODE 	=  299;
	
	private static final int CONNECTION_TIMEOUT 	=  90000;
	private static final int SOCKET_TIMEOUT 		=  60000;
	
	private static final String UTF_8_ENCODING = "UTF-8";	
	
	private enum Methods
	{
		GET,
		POST,
		PUT
	} 
	private enum Commands
	{		
		register_with_3rd_party_server,
		check_for_updates,
		report_install_complete,
		register_install_complete
	}
	
	public static final int JSON = 2;
	public static final int XML = 3;

	private static final String PATH_SEP = "/";	
	
	private String responsePhrase;
	private String response;
	private int respCode;

	private String url;
	private HashMap<String, String> params;
	private HashMap<String, String> headers;	

	public RestClient() {
		PrepareToSend();	
	}
	public void buildPostParams(){		
	}
	private void addParam(String key, String value){
		this.params.put(key, value);
	}
	private void addHeader(String key, String value){
		this.headers.put(key, value);
	}
	private String buildParams() throws UnsupportedEncodingException {
		Iterator<Entry<String, String>> it = params.entrySet().iterator();
		String res = "?";
		while (it.hasNext()) {
			Entry<String, String> pair = (Entry<String, String>) it.next();
			String add = pair.getKey() + "="
					+ URLEncoder.encode((String) pair.getValue(), UTF_8_ENCODING);
			if (params.size() > 1) {
				res += "&amp;" + add;
			} else {
				res += add;
			}
		}		
		if("?".equals(res))
		{
			res = "";
		}
		return res;
	}
	private JSONObject buildJSONParams(){
		JSONObject jsonObj = new JSONObject();
		Iterator<Entry<String, String>> it = params.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> pair = (Entry<String, String>) it.next();			
			try {
				jsonObj.put(pair.getKey(), pair.getValue());
			} catch (JSONException e) {
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, "Couldn't add json parameter");
			}
		}
		return jsonObj;
	}
	private String convertStreamToString(InputStream is) throws IOException  {
		String result = "";
		Reader reader = null;
		Writer writer = null;
		if (is != null) {
			writer = new StringWriter();
			char[] buffer = new char[1024];
			try {
				reader = new BufferedReader(new InputStreamReader(is,
						UTF_8_ENCODING));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
				reader.close();
				reader = null;
			} catch (UnsupportedEncodingException e) {
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, "UnsupportedEncodingException - " + e.getMessage());				
			}catch (Exception e) {
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, "Exception - " + e.getMessage());				
			}
			result = writer.toString();
			writer.close();
			writer = null;
		}
		Assert.assertEquals(null, reader);
		Assert.assertEquals(null, writer);				
		return result;
	}
	private HttpResponse SendRequestAndGetResponse(Commands aCmd) 
			throws ClientProtocolException, IOException
			{
		Methods method;
		switch (aCmd) 
		{
			case register_with_3rd_party_server:
			case report_install_complete:
			case register_install_complete:
				method = Methods.POST;				
				break;	
			case check_for_updates:
				method = Methods.GET;
				break;
			default:
				throw new ClientProtocolException();
		}

		HttpUriRequest request;
		switch (method) 
		{
			case GET:				
				StringBuilder args = new StringBuilder();
				String argsString = args.append(url).toString();
				request = new HttpGet(argsString);
				break;			
			case PUT:				
				request = new HttpPut(url + buildParams());
				break;				
			case POST:
				request = new HttpPost(url);
				addHeader("Content-type", "application/json");				
				((HttpPost)request).setEntity(new StringEntity(buildJSONParams().toString()));
				//((HttpPost)request).setEntity(new UrlEncodedFormEntity(mNameValuePairs));
				if(Parameters.ENABLE_DEBUG){ 
					Log.e(TAG, "SendRequestAndGetResponse msg = " + buildJSONParams().toString());
					Log.e(TAG, "SendRequestAndGetResponse url = " + url);
				}
				break;
			default:
				throw new ClientProtocolException();
		}

		// add headers 
		Iterator<Entry<String, String>> it = headers.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> header = (Entry<String, String>) it.next();
			request.addHeader(header.getKey(), header.getValue());
		}

		// exec request
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		int timeoutConnection = CONNECTION_TIMEOUT;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		// Set the default socket timeout (SOCKET_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = SOCKET_TIMEOUT;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient client = new DefaultHttpClient(httpParameters);
		return client.execute(request);
	} 
	private boolean callWebService(Commands aCmd) throws IllegalStateException, ClientProtocolException, IOException 
	{
		HttpResponse resp = SendRequestAndGetResponse(aCmd);
		this.respCode = resp.getStatusLine().getStatusCode();
		this.responsePhrase = resp.getStatusLine().getReasonPhrase(); 
		HttpEntity entity = resp.getEntity();
		if (entity != null) {
			InputStream is = entity.getContent();
			response = this.convertStreamToString(is);				
			is.close();
		}
		return IsResultOk(aCmd);		
	}
	private void PrepareToSend()
	{	
		if(null == params)
		{
			this.params = new HashMap<String, String>();
		}
		if(null == headers)
		{
			this.headers = new HashMap<String, String>();
		}
		params.clear();
		headers.clear();
		
		//AK: TODO use additional headers as needed
		/*
		addHeader("Accept", "application/json");
		addHeader("Content-type", "application/x-www-form-urlencoded");
		*/		
	}
	private boolean IsResultOk(Commands aCmd){
		switch (aCmd) 
		{
			case register_with_3rd_party_server:
			case check_for_updates:
				if(!(respCode >= MINIMUM_SUCCESS_CODE && respCode <= MAXIMUM_SUCCESS_CODE))
				{
					return true;
				}
				break;			
			default:
				return false;
		}
		return true;		
	}
	//AK: legacy code for checking version in server, will require update to uri if we wish to resume using it
	public String checkForNewVersion(String aUrl, String device)
	{
		PrepareToSend();
		StringBuilder sb = new StringBuilder(aUrl);	
		sb.append(RestClient.PATH_SEP).append(device);
		this.url = sb.toString();
		try
		{			
			if(callWebService(Commands.check_for_updates))
			{
				JSONObject responseObject = new JSONObject(response);
				String resp = responseObject.getString("message");
				return resp;
			}			
		}catch (Exception e)
		{			
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "Exception requesting for recommendation - " + e.getMessage());
			return null;
		}
		return null;
	}
	
	public boolean registerWith3rdPartyServer(String thirdPartyServerUrl, String regId, String deviceId){
		PrepareToSend();		
		this.url = thirdPartyServerUrl;
		try
		{
			if(deviceId == null)
				addParam(Parameters.REG_DEVICE_ID_PARAM, Parameters.NULL_DEVICE_ID);
			else addParam(Parameters.REG_DEVICE_ID_PARAM, deviceId);
			addParam(Parameters.REGISTRATION_ID_PARAM, regId);
			addParam(Parameters.REGISTRATION_VERSION_PARAM, Parameters.REGISTRATION_VERSION_DEFAULT);
			if(callWebService(Commands.register_with_3rd_party_server))
			{
				if(Parameters.ENABLE_DEBUG) Log.i(TAG, response);
				return true;
			}			
		}catch (Exception e)
		{			
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "Exception requesting for recommendation - " + e.getMessage());
		}
		return false;
	}
	
	public boolean registerInstallComplete(String thirdPartyServerUrl, String activityName, String deviceId){
		PrepareToSend();		
		this.url = Parameters.THIRD_PARTY_INSTALL_COMPLETE_URL;
		try
		{
			if(deviceId == null)
				addParam(Parameters.DEVICE_ID_PARAM, Parameters.NULL_DEVICE_ID);
			else addParam(Parameters.DEVICE_ID_PARAM, deviceId);
			addParam(Parameters.INSTALL_APP_NAME, activityName);
			if(callWebService(Commands.register_install_complete))
			{
				if(Parameters.ENABLE_DEBUG) Log.i(TAG, response);
				return true;
			}			
		}catch (Exception e)
		{			
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "Exception requesting for recommendation - " + e.getMessage());
			return false;
		}
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, response);
		return false;
	}
}
